<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;

class Clientecontroller extends Controller
{

    public function InicioCliente(Request $request)
    {
        $cliente = Cliente::all();
       return view('cliente.inicio')->with('cliente', $cliente);
    }

    public function Crearcliente(Request $request)
    {
        $cliente = Cliente::all();
        return view('cliente.crear')->with('cliente', $cliente);
    }
    
    public function Guardarcliente(Request $request){

        $this->validate($request, [
            'Nombre' => 'required',
            'Apellido'=> 'required',
            'Cedula'=> 'required',
            'Direccion'=> 'required',
            'Telefono'=> 'required',
            'Fecha_nacimiento'=> 'required',
            'Email'=> 'required'
        ]);

        $cliente = new Cliente;
        $cliente->Nombre=$request->Nombre;
        $cliente->Apellido=$request->Apellido;
        $cliente->Cedula=$request->Cedula;
        $cliente->Direccion=$request->Direccion;
        $cliente->Telefono=$request->Telefono;
        $cliente->Fecha_nacimiento=$request->Fecha_nacimiento;
        $cliente->Email=$request->Email;

        $cliente->save();
        return redirect()->route('list.cliente');
    }
}
